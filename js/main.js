document.getElementById("hello_text").textContent = "はじめてのJavaScript";

var count = 0;              // 描画回数
var cells;                  // ゲーム盤のブロック
var isFalling = false;       // 落ちているブロックの有無
var fallingBlockNum = 0;    // 落ちているブロックのインデックス番号
var isRightEdge = false;    // 落ちているブロックが右端にあるかないか
var isLeftEdge = false;     // 落ちているブロックが左端にあるかないか


// ブロックのパターン
var blocks = {
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
          [0, 1, 1], 
          [1, 1, 0]
        ]
      },
      z: {
        class: "z",
        pattern: [
          [1, 1, 0], 
          [0, 1, 1]
        ]
      },
      j: {
        class: "j",
        pattern: [
          [1, 0, 0], 
          [1, 1, 1]
        ]
      },
      l: {
        class: "l",
        pattern: [
          [0, 0, 1], 
          [1, 1, 1]
        ]
    }
};

loadTable();
document.addEventListener("keydown", onKeyDown);
setInterval(function() {
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";

    // ブロックが積み上がり切っていないかのチェック
    for (var row = 0; row < 2; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].className != "" && cells[row][col].blockNum != fallingBlockNum) {
                alert("game over");
            }
        }
    }
    if (hasFallingBlock()) {
        fallBlocks();
    }
    else {
        deleteRow();
        generateBlock();
    }
}, 250);

function loadTable() {
    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    for (var row = 0; row < 20; row++) {
        cells[row] = [];
        for (var col = 0; col < 10; col++) {
            cells[row][col] = td_array[index];
            index++;
        }
    }
}

function fallBlocks() {
    isRightEdge = false;
    isLeftEdge = false;
    // 底についていないか
    for (var col = 0; col < 10; col++) {
        if (cells[19][col].blockNum == fallingBlockNum) {
            isFalling = false;
            return;
        }
    }
    // 1マス下に別のブロックがないか
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum == fallingBlockNum) {
                if (cells[row+1][col].className != "" && cells[row+1][col].blockNum != fallingBlockNum) {
                    isFalling = false;
                    return;
                }
            }
        }
    }

    // 1マス右にブロックがないか
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 9; col++) {
            if (cells[row][col].blockNum == fallingBlockNum) {
                if (cells[row][col+1].className != "" && cells[row][col+1].blockNum != fallingBlockNum) {
                    isRightEdge = true;
                }
            }
        }
        if (cells[row][9].blockNum == fallingBlockNum) {
            isRightEdge = true;
        }
    }

    // 1マス左にブロックがないか
    for (var row = 0; row < 20; row++) {
        for (var col = 1; col < 10; col++) {
            if (cells[row][col].blockNum == fallingBlockNum) {
                if (cells[row][col-1].className != "" && cells[row][col-1].blockNum != fallingBlockNum) {
                    isLeftEdge = true;
                }
            }
        }
        if (cells[row][0].blockNum == fallingBlockNum) {
            isLeftEdge = true;
        }
    }

    // 上の行を下の行に落とす
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum == fallingBlockNum) {
                cells[row+1][col].className = cells[row][col].className;
                cells[row+1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function hasFallingBlock() {
    return isFalling;
}

function hasRightEdgingBlock() {
    return isRightEdge;
}

function hasLeftEdgingBlock() {
    return isLeftEdge;
}

function deleteRow() {
    // そろっている行を消す
    for (var row = 19; row >= 0; row--) {
        var canDelete = true;
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].className == "") {
                canDelete = false;
            }
        }
        if (canDelete) {
        // 1行消す
            for (var col = 0; col < 10; col++) {
                cells[row][col].className = "";
            }
            // 上の行のブロックをすべて1マス落とす
            for (var downRow = row - 1; downRow >= 0; downRow--) {
                for (var col = 0; col < 10; col++) {
                    cells[downRow+1][col].className = cells[downRow][col].className;
                    cells[downRow+1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                }
            }
        }
    }
}

function generateBlock() {
    isRightEdge = false;
    isLeftEdge = false;
    // 1. ブロックパターンからランダムに一つ選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;

    // 2. 選んだパターンを元にブロックを配置する
    var pattern = nextBlock.pattern;
    for (var row= 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col] == 1) {
                cells[row][col+3].className = nextBlock.class;
                cells[row][col+3].blockNum = nextFallingBlockNum;
            }
        }
    }

    // 3. 落下中のブロックがあるとする
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

function onKeyDown(event) {
    if (event.keyCode == 37) {
        moveLeft();
    }
    else if (event.keyCode == 39) {
        moveRight();
    }
}

function moveRight() {
    // 右端にブロックあるかないか
    if (hasRightEdgingBlock()) {
        return;
    }

    // ブロックを右に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum == fallingBlockNum && cells[row][col+1].blockNum == null) {
                cells[row][col+1].className = cells[row][col].className;
                cells[row][col+1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveLeft() {
    // 左端にブロックがあるかないか
    if (hasLeftEdgingBlock()) {
        return;
    }
    // ブロックを左に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum == fallingBlockNum && cells[row][col-1].blockNum == null) {
                cells[row][col-1].className = cells[row][col].className;
                cells[row][col-1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}
